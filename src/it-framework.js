/* eslint-disable global-require */
import './components/jQuery.extends';

(($) => {
  // validation
  if ($.ui === undefined) throw new Error('Jquery UI not Loaded');
  window.IT = window.IT || {};
  Object.assign(window.IT, {
    defaultSettings: {
      // Identity
      id: '',

      // Layout
      disabled: false,
      hidden: false,
      label: {
        type: 'default',
        text: '',
      },
      tooltip: null,
      popover: null,
      badge: '',
      cls: '',
      style: {},
      role: '',

      // Positioning
      width: '',
      height: '',
      top: '',
      bottom: '',
      left: '',
      right: '',
      margin: '',

      // Rendering
      renderTo: '',
    },
    CreateComponent(settings) {
      var { xtype } = settings,
        result = null;

      switch (xtype) {
        case 'column':
          result = new IT.Column(settings);
          break;
        case 'panel':
          result = new IT.Panel(settings);
          break;
        case 'layout':
          result = new IT.Layout(settings);
          break;
        case 'button':
          result = new IT.Button(settings);
          break;
        case 'buttongroup':
          result = new IT.ButtonGroup(settings);
          break;
        case 'dropdown':
          result = new IT.DropDown(settings);
          break;
        case 'navbar':
          result = new IT.NavBar(settings);
          break;
        case 'navs':
          result = new IT.Navs(settings);
          break;
        case 'editor':
          result = new IT.Editor(settings);
          break;
        case 'combobox':
          result = new IT.ComboBox(settings);
          break;
        case 'form':
          result = new IT.Form(settings);
          break;
        case 'label':
          result = new IT.Label(settings);
          break;
        case 'textbox':
          result = new IT.TextBox(settings);
          break;
        case 'checkbox':
          result = new IT.CheckBox(settings);
          break;
        case 'imagebox':
          result = new IT.ImageBox(settings);
          break;
        case 'html':
          result = new IT.HTML(settings);
          break;
        case 'tinymce':
          result = new IT.TinyMCE(settings);
          break;
        case 'frame':
          result = new IT.Frame(settings);
          break;
        case 'progress':
          result = new IT.ProgressBar(settings);
          break;
        case 'metrics':
          result = new IT.Metrics(settings);
          break;
        default:
          throw new Error('xtype not found');
      }
      return result;
    },
    EventListener: require('./components/EventListener').default,
    BaseComponent: require('./components/BaseComponent').default,
    ContainerComponent: require('./components/ContainerComponent').default,
    Modal: require('./components/Modal/_index').default,
    HTML: require('./components/HTML/_index').default,
    NavBar: require('./components/NavBar/_index').default,
    Button: require('./components/Button/_index').default,
    MessageBox: require('./components/MessageBox/_index').default,

    InputComponent: require('./components/InputComponent/_index').default,

    DropDown: require('./components/DropDown/_index').default,
    CheckBox: require('./components/CheckBox/_index').default,
    ComboBox: require('./components/ComboBox/_index').default,

    DataSource: require('./components/DataSource/_index').default,
    Store: require('./components/Store/_index').default,
    DataTable: require('./components/DataTable/_index').default,

    Panel: require('./components/Panel/_index').default,
  });
})(jQuery);
