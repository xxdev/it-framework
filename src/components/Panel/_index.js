export default function Panel(params) {
  implement(this, IT.BaseComponent, IT.ContainerComponent);

  var me = this;
  var arTools = {};

  me.element = $('<div>', { class: 'panel panel-default' });
  me.header = $('<div>', { class: 'panel-heading' });
  me.body = $('<div>', { class: 'panel-body' });
  me.footer = $('<div>', { class: 'panel-footer' });
  me.tools = $('<div>', { class: 'panel-tools' });

  me.element.append(me.header).append(me.body).append(me.footer);

  me.init = (par) => {
    me.settings = $.extend(
      {},
      IT.defaultSettings,
      {
        iconCls: '',
        title: '',
        footer: '',
        size: '',
        fullSize: false,
        padding: true,
        items: [],
        tools: [],
      },
      par,
    );

    me.items = me.settings.items;
    me.itemArray = [];
    me.itemObject = {};
  };

  me.createComponent = () => {
    me.initComponent(me.element);
    me.createItems();
    me.createTools();
    me.setHeader(me.settings.title);
    me.setFooter(me.settings.footer);

    me.element.setClass(me.settings.size);
    me.element.setClass('fullsize', me.settings.fullSize);
    me.body.setClass('no-padding', !me.settings.padding);
    me.header.setIcon(me.settings.iconCls);
    me.header.setBadge(me.settings.badge);
    me.header.setLabel(me.settings.label);
    me.header.append(me.tools);
  };

  me.createItems = () => {
    for (var i = 0; i < me.items.length; i += 1) {
      if (me.items[i]) {
        var item = me.getComponent(me.items[i]);
        if (item) item.renderTo(me.body);
        me.addItem(item, i);
      }
    }
  };

  me.createTools = () => {
    var { tools } = me.settings;
    for (var i = 0; i < tools.length; i += 1) {
      if (tools[i]) {
        var tool = me.getComponent(tools[i]);
        if (tool) {
          tool.renderTo(me.tools);
          if (typeof tool.getId === 'function') {
            arTools[tool.getId] = tool;
          }
        }
      }
    }
  };

  me.getTool = (id) => {
    return arTools[id];
  };

  me.init(params);
  me.createComponent();
  me.renderTo(me.settings.renderTo);

  return me;
}
