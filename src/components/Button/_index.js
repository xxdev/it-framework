export default function Button(params) {
  implement(this, IT.BaseComponent);

  var me = this;

  me.element = $('<a>', { href: 'javascript:void(0)', class: 'btn' });

  me.init = (args) => {
    me.settings = $.extend(
      {},
      IT.defaultSettings,
      {
        text: '',
        iconCls: '',
        iconPosition: 'left',
        handler() {},
      },
      args,
    );
  };

  me.createComponent = () => {
    me.initComponent(me.element);

    me.element.html(me.settings.text);
    me.element.setIcon(me.settings.iconCls, me.settings.iconPosition);
    me.element.setBadge(me.settings.badge);
    me.element.setLabel(me.settings.label);
    me.element.setClass('btn-default', empty(me.settings.cls));
    me.element.setClass('btn-icon-only', empty(me.settings.text));
    me.element.click(() => {
      if (me.settings.onTable) {
        $(this).closest('td').trigger('click');
      }

      if (!me.settings.hidden) {
        var { handler } = me.settings;
        if (typeof handler === 'function') {
          handler.call();
        } else if (typeof handler === 'string') {
          window[handler]();
        }
      }
    });
  };

  me.init(params);
  me.createComponent();
  me.renderTo(me.settings.renderTo);

  return me;
}
