export default function MessageBox(params) {
  var me = this;
  var settings = $.extend(
    {
      title: 'title',
      type: 'success',
      size: 'modal-msg',
      text: 'messagebox',
      buttons: [],
    },
    params,
  );
  me.modal = null;

  if (settings.buttons.length < 1) {
    settings.buttons = [
      {
        xtype: 'button',
        iconCls: 'check-circle',
        text: 'Ok',
        position: 'right',
        handler() {
          me.modal.close();
        },
      },
    ];
  }

  var container = $('<div>', { class: 'IT-msg-container' });
  var colImg = $('<div>', { class: 'IT-msg-wrapper' });
  var colMsg = $('<div>', { class: 'IT-msg-wrapper', html: settings.text });
  var img = $('<div>', { class: `IT-msg IT-msg-${settings.type}` });

  colImg.append(img);
  container.append(colImg);
  container.append(colMsg);

  me.modal = new IT.Modal({
    title: settings.title,
    size: settings.size,
    width: settings.width,
    footer: {
      xtype: 'navbar',
      collapse: 'none',
      items: settings.buttons,
    },
    items: [
      {
        xtype: 'html',
        content: container,
      },
    ],
  });
  return me.modal;
}
