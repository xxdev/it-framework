export default function BaseComponent() {
  var me = this;
  me.base = null;

  me.initComponent = (element) => {
    element.setId(me.settings.id);
    element.setDisable(me.settings.disabled);
    element.setClass(me.settings.cls);
    element.setAttr('role', me.settings.role);
    me.initLayout(element);
    me.initTooltip(element);
  };

  me.initLayout = (element) => {
    element.setHidden(me.settings.hidden);
    element.setCss('width', me.settings.width);
    element.setCss('height', me.settings.height);
    element.setCss('top', me.settings.top);
    element.setCss('left', me.settings.left);
    element.setCss('right', me.settings.right);
    element.setCss('bottom', me.settings.bottom);
    element.setCss('margin', me.settings.margin);
    element.css(me.settings.style);
  };

  me.initTooltip = (element) => {
    if (me.settings.tooltip != null) {
      element.tooltip(me.settings.tooltip);
    }
    if (me.settings.popover != null) {
      element.popover(me.settings.popover);
    }
  };

  me.addClass = (c) => {
    me.element.addClass(c);
  };

  me.removeClass = (c) => {
    me.element.removeClass(c);
  };

  me.clearClass = () => {
    me.element.removeAttr('class');
  };

  me.append = (o) => {
    me.element.append(o);
  };

  me.prepend = (o) => {
    me.element.prepend(o);
  };

  me.wrap = (w) => {
    me.element.wrap(w);
  };

  me.show = () => {
    if (me.base != null) {
      me.base.show();
    } else {
      me.element.show();
    }
  };

  me.hide = () => {
    if (me.base != null) {
      me.base.hide();
    } else {
      me.element.hide();
    }
  };

  me.focus = () => {
    me.element.focus();
  };

  me.setDisabled = (d) => {
    me.element.setDisable(d);
  };

  me.setSettings = (s) => {
    me.settings = s;
    me.createComponent();
  };

  me.getSettings = () => {
    return me.settings;
  };

  me.setId = (i) => {
    me.id = i;
    me.element.attr('id', i);
  };

  me.getId = () => {
    return me.element.attr('id');
  };

  me.setElement = (e) => {
    me.element = e;
    me.createComponent();
  };

  me.getElement = () => {
    return me.element;
  };

  me.setSize = (c) => {
    var size = c || me.settings.size;
    if (!empty(size)) {
      me.element.wrap($('<div>', { class: `${size} size-wrapper` }));
    }
  };

  me.renderTo = (p) => {
    if (!empty(p)) {
      me.element.appendTo(p);

      me.setSize();
      me.parent = p;
    }
  };
}
