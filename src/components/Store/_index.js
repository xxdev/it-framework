export default function Store(params) {
  implement(this, IT.EventListener);

  var me = this;

  me.beforeLoad = (act) => {
    me.addEvent('beforeLoad', act);
  };
  me.afterLoad = (act) => {
    me.addEvent('afterLoad', act);
  };
  me.completeLoad = (act) => {
    me.addEvent('completeLoad', act);
  };
  me.onLoad = (act) => {
    me.addEvent('onLoad', act);
  };
  me.onError = (act) => {
    me.addEvent('onError', act);
  };
  me.onChange = (act) => {
    me.addEvent('onChange', act);
  };

  me.params = {};
  me.storeData = null;
  me.dataChanged = [];
  me.isSaved = false;

  me.init = () => {
    me.settings = $.extend(
      {
        type: 'json',
        url: '',
        data: {},

        params: {
          start: 0,
          limit: 20,
          orderBy: '',
          sortBy: '',
        },
      },
      params,
    );
  };

  me.empty = () => {
    me.dataChanged = [];
    me.storeData = { rows: [], totalRows: 0 };
    me.fireEvent('onLoad', [me.storeData, me.params]);
  };

  me.load = (opt) => {
    me.fireEvent('beforeLoad', [me.storeData]);

    // var opt = opt || {};

    switch (me.settings.type) {
      case 'json':
        me.params = $.extend(me.settings.params, opt.params);
        me.dataChanged = [];

        var global = params.global !== undefined ? params.global : true;

        $.ajax({
          type: 'POST',
          url: me.settings.url,
          data: me.params,
          global,
          dataType: me.settings.type,
          success: (data) => {
            if (!data.success && data.redirect !== undefined) {
              windo.location.href = data.redirect;
            }

            if (data.rows !== undefined && data.totalRows !== undefined) {
              me.storeData = data;
              me.fireEvent('onLoad', [me.storeData, me.params]);
            } else {
              me.storeData = {};
              me.fireEvent('onError', [
                {
                  status: false,
                  message: 'Format Data Tidak Sesuai',
                },
              ]);
            }
          },
          error: () => {
            me.fireEvent('onError', [
              {
                status: false,
                message: 'Data Tidak Ditemukan',
              },
            ]);
          },
          complete: () => {
            me.fireEvent('afterLoad', [me.storeData]);
            me.fireEvent('completeLoad', ['satu', 'dua']);
          },
        });
        break;

      case 'array':
        if (me.settings.data.rows !== undefined && me.settings.data.totalRows !== undefined) {
          me.storeData = me.settings.data;
          me.fireEvent('onLoad', [me.storeData, me.params]);
        } else {
          me.fireEvent('onError', [
            {
              status: false,
              message: 'Data JSON Tidak Ditemukan',
            },
          ]);
        }
        me.fireEvent('afterLoad', [me.storeData]);
        me.fireEvent('completeLoad', ['satu', 'dua']);
        break;
      default:
        break;
    }
  };

  me.searchData = (data, key, value) => {
    var index = null;

    for (var i = 0; i < data.length; i += 1) {
      if (data[i][key] === value) {
        index = i;
        break;
      }
    }

    return index;
  };

  var isMoney = (value) => {
    var m = value.replace(/[$,]/g, '').replace(/\./g, '').replace(/,/g, '.');

    return !Number.isNaN(m);
  };

  var isDate = (value) => {
    var d = new Date(value);

    return !Number.isNaN(d);
  };

  me.sort = (prop, asc) => {
    me.storeData.rows = me.storeData.rows.sort((a, b) => {
      var valueA = a[prop];
      var valueB = b[prop];
      if (!Number.isNaN(valueA) && !Number.isNaN(valueB)) {
        valueA = parseFloat(valueA);
        valueB = parseFloat(valueB);
      } else if (isDate(valueA) && isDate(valueB)) {
        valueA = new Date(valueA);
        valueB = new Date(valueB);
      } else if (isMoney(valueA) && isMoney(valueB)) {
        valueA = parseFloat(valueA.replace(/[$,]/g, '').replace(/\./g, '').replace(/,/g, '.'));
        valueB = parseFloat(valueB.replace(/[$,]/g, '').replace(/\./g, '').replace(/,/g, '.'));
      }

      if (asc === 'ASC') return valueA > valueB;
      return valueB > valueA;
    });

    me.fireEvent('onLoad', [me.storeData, me.params]);
    me.fireEvent('afterLoad', [me.storeData]);
    me.fireEvent('completeLoad', ['satu', 'dua']);
  };

  me.getParams = () => {
    return me.params;
  };
  me.getData = () => {
    return me.storeData;
  };
  me.setData = (d) => {
    me.settings.data = d;
  };
  me.getSetting = () => {
    return me.settings;
  };

  me.getChanged = (index, column, data) => {
    if ($.trim(me.storeData.rows[index][column]) !== $.trim(data)) {
      rows = $.extend({ indexRow: index }, me.storeData.rows[index]);

      if (me.searchData(me.dataChanged, 'indexRow', index) === null) {
        me.dataChanged.push(rows);
      }

      me.dataChanged[me.searchData(me.dataChanged, 'indexRow', index)][column] = data;
      me.fireEvent('onChange', [{ index, data: [me.dataChanged[me.searchData(me.dataChanged, 'indexRow', index)]] }]);

      return true;
    }

    if (me.searchData(me.dataChanged, 'indexRow', index) !== null) {
      me.dataChanged[me.searchData(me.dataChanged, 'indexRow', index)][column] = data;
    }
    return false;
  };

  me.init();

  return me;
}
