export default function EventListener(baseEvent = []) {
  var me = this;
  me.baseEvents = empty(baseEvent)
    ? ['blur', 'change', 'click', 'dblclick', 'focus', 'hover', 'keydown', 'keypress', 'keyup', 'show', 'hide']
    : baseEvent;
  me.events = {};
  me.addEvent = (e, f) => {
    if (typeof f === 'function') {
      me.events[e] = f;
    }
  };

  me.fireEvent = (events, args, res) => {
    var result = res || me;

    if (Object.prototype.hasOwnProperty.call(me.events, events) && typeof me.events[events] === 'function') {
      me.events[events].apply(result, args);
    }

    if (Object.prototype.hasOwnProperty.call(me.settings, events) && typeof me.settings[events] === 'function') {
      me.settings[events].apply(result, args);
    }
  };

  me.setEvent = (element) => {
    $.each(me.baseEvents, (index, value) => {
      var on = `on${value.substring(0, 1).toUpperCase()}${value.substring(1, value.length).toLowerCase()}`;

      me[value] = () => {
        element.trigger(value);
      };

      me[on] = (act) => {
        me.addEvent(on, act);
      };

      element.on(value, () => {
        me.fireEvent(value, [], this);
        me.fireEvent(on, []);
      });
    });

    return result;
  };
}
