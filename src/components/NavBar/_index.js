export default function NavBar(params) {
  var me = this;
  implement(this, IT.BaseComponent, IT.ContainerComponent);

  me.element = $('<div>', { class: 'navbar navbar-default' });
  me.container = $('<div>', { class: 'container-fluid' });
  me.navbarLeft = $('<form>', { class: 'navbar-form' });
  me.navbarRight = $('<form>', { class: 'navbar-form' });
  me.navbarHeader = $(
    [
      '<div class="navbar-header">',
      '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse">',
      '<span class="sr-only">Toggle navigation</span>',
      '<span class="icon-bar"></span>',
      '<span class="icon-bar"></span>',
      '<span class="icon-bar"></span>',
      '</button>',
      '</div>',
    ].join(''),
  );

  me.table = $(
    [
      '<table width="100%">',
      '<tr>',
      '<td class="tableLeft" align="left"></td>',
      '<td class="tableRight" align="right"></td>',
      '</tr>',
      '</table>',
    ].join(''),
  );

  me.navbarLeft.appendTo(me.table.find('.tableLeft'));
  me.navbarRight.appendTo(me.table.find('.tableRight'));
  me.element
    .append(me.container)
    // .append(me.navbarHeader)
    .append(me.table);

  me.navbarRight.hover(() => {
    me.element.stop().animate({ scrollLeft: me.table.outerWidth() + 20 }, 500);
  });

  me.navbarLeft.hover(() => {
    me.element.stop().animate({ scrollLeft: 0 }, 500);
  });

  me.init = (args) => {
    me.settings = $.extend(
      {},
      IT.defaultSettings,
      {
        collapse: 'right',
        icon: '',
        items: [],
      },
      args,
    );

    me.items = me.settings.items;
    me.itemArray = [];
    me.itemObject = {};
  };

  me.createComponent = () => {
    me.initComponent(me.element);
    me.navbarLeft.setClass(
      'collapse show navbar-collapse',
      me.settings.collapse === 'both' || me.settings.collapse === 'left',
    );
    me.navbarRight.setClass(
      'collapse show navbar-collapse',
      me.settings.collapse === 'both' || me.settings.collapse === 'right',
    );
    me.createItems();
    me.navbarHeader.find('.navbar-toggle').setAttr('data-target', `#${me.getId()} .navbar-collapse`);

    if (!empty(me.settings.icon)) {
      me.navbarHeader.find('.navbar-toggle').html($('<span>', { class: `fa fa-${me.settings.icon}` }));
    }

    if (me.settings.collapse === 'none') {
      me.navbarHeader.remove();
    }
  };

  me.createItems = () => {
    var itemLeft =
        me.settings.collapse !== 'both' && me.settings.collapse !== 'left'
          ? $('<div>', { class: 'form-group' }).appendTo(me.navbarLeft)
          : null,
      itemRight =
        me.settings.collapse !== 'both' && me.settings.collapse !== 'right'
          ? $('<div>', { class: 'form-group' }).appendTo(me.navbarRight)
          : null,
      i,
      item,
      itemWrapper;

    for (i = 0; i < me.items.length; i += 1) {
      if (me.items[i] !== null) {
        item = me.getComponent(me.items[i]);
        itemWrapper = $('<div>', { class: 'form-group' });

        if (item) {
          var { navClass } = item.getSettings();
          itemWrapper.addClass(navClass);

          if (item.getSettings().position === 'right') {
            if (me.settings.collapse === 'both' || me.settings.collapse === 'right') {
              item.renderTo(itemWrapper);
              itemWrapper.appendTo(me.navbarRight);
            } else {
              item.renderTo(itemRight);
            }
          } else if (me.settings.collapse === 'both' || me.settings.collapse === 'left') {
            item.renderTo(itemWrapper);
            itemWrapper.appendTo(me.navbarLeft);
          } else {
            item.renderTo(itemLeft);
          }

          itemWrapper.find('.dropdown-menu').addClass('onNavBar');

          if (itemRight != null) itemRight.find('.dropdown-menu').addClass('onNavBar');
          if (itemLeft != null) itemLeft.find('.dropdown-menu').addClass('onNavBar');
        }
        me.addItem(item, i);
      }
    }
  };

  $(window).off('show.bs.dropdown');
  $(window).on('show.bs.dropdown', (e) => {
    var eOffset,
      dropdownMenu = $(e.target).find('.dropdown-menu');
    if (dropdownMenu.hasClass('onNavBar')) {
      $('body').append(dropdownMenu.detach());
      eOffset = $(e.target).offset();
      dropdownMenu.css({
        display: 'block',
        top: eOffset.top + $(e.target).outerHeight(),
        left: eOffset.left,
      });
    }
  });

  $(window).off('hide.bs.dropdown');
  $(window).on('hide.bs.dropdown', (e) => {
    if (dropdownMenu.hasClass('onNavBar')) {
      $(e.target).append(dropdownMenu.detach());
      dropdownMenu.hide();
    }
  });

  me.init(params);
  me.createComponent();
  me.renderTo(me.settings.renderTo);

  return me;
}
