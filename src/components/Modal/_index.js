export default function Modal(params) {
  var me = this;
  implement(this, IT.BaseComponent, IT.ContainerComponent, IT.EventListener);
  me.afterShow = (act) => {
    me.addEvent('afterShow', act);
  };
  me.afterClose = (act) => {
    me.addEvent('afterClose', act);
  };
  me.backdrop = $('<div>', { class: 'modal-backdrop fade ' }).appendTo('body');
  me.element = $('<div>', { class: 'modal fade d-block ' });

  me.dialog = $('<div>', { class: 'modal-dialog modal-dialog-centered' });
  me.content = $('<div>', { class: 'modal-content' });
  me.header = $('<div>', {
    class: 'modal-header ',
    css: {
      cursor: 'move',
    },
  });

  me.body = $('<div>', { class: 'modal-body' });
  me.footer = $('<div>', { class: 'modal-footer' });
  me.content.append(me.header);
  me.content.append(me.body);
  me.content.append(me.footer);
  me.dialog.append(me.content);
  me.element.append(me.dialog);
  me.zIndex = $('.modal').last().css('z-index') + 50;
  me.init = (args) => {
    me.settings = $.extend(
      {},
      IT.defaultSettings,
      {
        iconCls: '',
        title: '',
        footer: '',
        size: '',
        renderTo: 'body',
        center: true,
        padding: true,
        items: [],
      },
      args,
    );
    me.items = me.settings.items;
    me.itemArray = [];
    me.itemObject = {};
  };
  me.createComponent = () => {
    me.initLayout(me.dialog);
    me.initLayout(me.content);
    me.createItems();
    me.setHeader(me.settings.title);
    me.setFooter(me.settings.footer);
    me.dialog.setClass(me.settings.size);
    me.content.setClass(me.settings.size);
    me.header.setIcon(me.settings.iconCls);
    me.header.setBadge(me.settings.badge);
    me.header.setLabel(me.settings.label);
    me.header.append('<button type="button" class="close" data-dismiss="modal">&times;</button>');
    me.header.find('button').click(() => {
      me.close();
    });
    me.content.draggable({ handle: '.modal-header' });
    me.body.setClass('no-padding', !me.settings.padding);
    me.element.setClass('fullsize', !empty(me.settings.height));
  };
  me.createItems = () => {
    var i, item;
    for (i = 0; i < me.items.length; i += 1) {
      if (me.items[i] !== null) {
        item = me.getComponent(me.items[i]);
        if (item) item.renderTo(me.body);
        me.addItem(item, i);
      }
    }
  };
  me.center = (element) => {
    var modals;
    if (typeof element.hasClass === 'function' && element.hasClass('modal')) {
      if (element.length) {
        modals = element;
      } else {
        modals = $('.modal-vcenter:visible');
      }
      modals.each(() => {
        var clone = $(this).clone().css('display', 'block').appendTo('body'),
          top = Math.round((clone.height() - clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        clone.remove();
        $(this).find('.modal-content').css('margin-top', top);
      });
      $(window).on('resize', me.center);
    }
  };
  me.show = () => {
    me.backdrop.addClass('show');
    me.element.addClass('show');
    if (!me.element.hasClass('in')) {
      setTimeout(() => {
        me.backdrop.setCss('z-index', me.zIndex + 1);
        me.element.setCss('z-index', me.zIndex + 2);
        me.backdrop.addClass('in');
        me.element.addClass('in');
        $('body').addClass('modal-open');
        // if (me.settings.center && $(window).width() > 800) {
        //   me.center(me.element);
        // }
        me.fireEvent('afterShow', [me, me.element]);
      }, 500);
    }
  };
  me.close = () => {
    if (me.element.hasClass('show')) {
      me.backdrop.removeClass('show');
      me.element.removeClass('show');
    }

    if (me.element.hasClass('in')) {
      setTimeout(() => {
        me.element.removeClass('in');
        me.backdrop.removeClass('in');
        me.element.closest('div.size-wrapper').remove();
        me.element.remove();
        me.backdrop.remove();
        if ($('body').find('.modal').length <= 0) {
          $('body').removeClass('modal-open');
        }
        if (me.element) me.fireEvent('afterClose', [me, me.element]);
      }, 500);
    }
  };
  me.init(params);
  me.createComponent();
  me.renderTo(me.settings.renderTo);
  setTimeout(me.show, 200);
  return me;
}
