export default function DataSource() {
  var me = this;

  me.getDataSource = (params) => {
    me.settings.datasource.params = params || me.settings.datasource.params;
    me.params = me.settings.datasource.params;

    if (me.settings.datasource.type === 'array') {
      //   var row = me.settings.datasource.data;
      if (me.settings.datasource.data !== null) {
        me.data = me.settings.datasource.data;
        me.processDatasource();
      }
    } else if (me.settings.datasource.type === 'ajax') {
      $.ajax({
        url: me.settings.datasource.url,
        type: me.settings.datasource.method || 'POST',
        data: me.settings.datasource.params || {},
        dataType: 'json',
        success: (data) => {
          if (!data.success && data.redirect !== undefined) {
            window.location.href = data.redirect;
          }

          me.data = data.rows;
          me.processDatasource();
        },
      });
    }
  };

  me.setDataSource = (d) => {
    $.extend(me.settings.datasource, d);
    me.getDataSource();
  };
}
