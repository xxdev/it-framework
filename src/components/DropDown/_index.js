export default function DropDown(params) {
  implement(this, IT.BaseComponent, IT.ContainerComponent);

  var me = this;

  var caret = $('<span>', { class: 'caret' });

  me.button = $('<a>', { href: 'javascript:void(0)', class: 'btn btn-default' });
  me.split = $('<a>', { href: 'javascript:void(0)', class: 'btn btn-default btn-split', html: caret });
  me.dropdown = $('<ul>', { class: 'dropdown-menu', role: 'menu' });
  me.element = $('<div>', { class: 'dropdown btn-group' });

  me.init = (par) => {
    me.settings = $.extend(
      {},
      IT.defaultSettings,
      {
        text: '',
        iconCls: '',
        iconPosition: 'left',
        direction: 'left',
        split: false,
        form: false,
        items: [],
      },
      par,
    );

    me.items = me.settings.items;
    me.itemArray = [];
    me.itemObject = {};
  };

  me.createComponent = () => {
    me.initLayout(me.element);
    me.id = me.settings.id ? me.settings.id : tools.makeRandomId();

    me.button.html(me.settings.text);
    me.button.setClass(me.settings.cls);
    me.button.setDisable(me.settings.disabled);
    me.button.setIcon(me.settings.iconCls, me.settings.iconPosition);
    me.button.setId(`${me.id}-button`);
    me.button.setClass('btn-icon-only', empty(me.settings.text));
    me.split.setId(`${me.id}-split`);
    me.split.setClass(me.settings.cls);
    me.split.setDisable(me.settings.disabled);

    me.dropdown.setId(`${me.id}-dropdown`);
    me.dropdown.setClass(`dropdown-menu-${me.settings.direction}`);
    me.element.setId(me.id);

    if (me.settings.form) {
      me.dropdown.setClass('dropdown-form');
    }

    if (me.settings.split) {
      me.split.setClass('dropdown-toggle');
      me.split.setAttr('data-toggle', 'dropdown');
      me.split.setAttr('aria-expanded', 'false');

      me.element.append(me.button);
      me.element.append(me.split);
      me.element.append(me.dropdown);

      me.button.click(() => {
        var { handler } = me.settings;
        if (handler === '') {
          handler.call();
        } else if (typeof handler === 'string') {
          window[handler]();
        }
      });
    } else {
      me.button.setClass('dropdown-toggle');
      me.button.setAttr('data-toggle', 'dropdown');
      me.button.setAttr('aria-expanded', 'false');
      me.button.append(' ').append(caret);

      me.element.append(me.button);
      me.element.append(me.dropdown);
    }

    me.createItems();
  };

  me.createItems = () => {
    for (var i = 0; i < me.items.length; i += 1) {
      if (me.items[i]) {
        var item = me.getComponent(me.items[i]),
          itemWrapper = $('<li>', { role: 'presentation' });

        if (item) {
          item.clearClass();
          item.renderTo(itemWrapper);
        }

        me.addItem(item, i);
        me.dropdown.append(itemWrapper);
      }
    }
  };

  me.setDisabled = (d) => {
    me.button.setDisable(d);
    me.split.setDisable(d);
  };

  me.init(params);
  me.createComponent();
  me.renderTo(me.settings.renderTo);

  return me;
}
