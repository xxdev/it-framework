String.prototype.format = function () {
  var tmp = arguments;
  return this.replace(/\{(\d+)\}/g, function (m, i) {
    return tmp[i];
  });
};
Array.prototype.remove = function (name, value) {
  var rest = $.grep(this, function (item) {
    return item[name] !== value;
  });

  this.length = 0;
  this.push.apply(this, rest);
  return this;
};

Object.assign($.fn, {
  serializeObject: function () {
    var objectArray = {};
    var sa = this.serializeArray();

    $.each(sa, function () {
      if (objectArray[this.name] !== undefined) {
        if (!objectArray[this.name].push) {
          objectArray[this.name] = [objectArray[this.name]];
        }

        objectArray[this.name].push(this.value || '');
      } else {
        objectArray[this.name] = this.value || '';
      }
    });
    return objectArray;
  },
  setCenter: function (params) {
    var defaults = {
      topBottom: true,
      leftRight: true,
    };

    var params = $.extend(defaults, params);

    return this.each(function () {
      var me = $(this),
        parent = params.parent || $(document);

      if (params.leftRight) {
        me.css('left', (parent.innerWidth() - $(this).outerWidth()) / 2);
      }

      if (params.topBottom) {
        me.css('top', (parent.innerHeight() - $(this).outerHeight()) / 2);
      }
    });
  },
  setCss: function (attribute, value) {
    var attribute = attribute,
      value = value;

    return this.each(function () {
      if (value != undefined && (!empty(value) || value > 0 || value === true)) {
        $(this).css(attribute, value);
      }
    });
  },
  setAttr: function (attribute, value) {
    var attribute = attribute,
      value = value;

    return this.each(function () {
      if (value != undefined && (!empty(value) || value > 0 || value === true)) {
        $(this).attr(attribute, value);
      }
    });
  },
  setId: function (value) {
    var value = value || tools.makeRandomId();

    return this.each(function () {
      $(this).attr('id', value);
    });
  },
  setDisable: function (value, disabledClass) {
    var value = value,
      disabledClass = disabledClass || 'disabled';

    return this.each(function () {
      if (value === true) {
        $(this).addClass(disabledClass);
        $(this).attr('disabled', true);
      } else {
        $(this).removeClass(disabledClass);
        $(this).removeAttr('disabled');
      }
    });
  },
  setHidden: function (value) {
    var value = value;

    return this.each(function () {
      if (value === true) {
        $(this).addClass('hidden');
        $(this).attr('hidden', true);
      } else {
        $(this).removeClass('hidden');
        $(this).removeAttr('hidden');
      }
    });
  },
  setIcon: function (icon, iconAlign) {
    var icon = icon,
      iconAlign = iconAlign || 'left';

    return this.each(function () {
      if (icon != undefined && !empty(icon)) {
        var base = 'fa';

        if ($.inArray(icon.substring(0, 3), ['far', 'fas', 'fab']) !== -1) {
          base = icon.substring(0, 3);
          icon = icon.substring(4, icon.length);
        }

        if (iconAlign === 'left') {
          $(this).prepend($('<i>', { class: 'icon-left ' + base + ' fa-' + icon }));
        } else {
          $(this).append($('<i>', { class: 'icon-right ' + base + ' fa-' + icon }));
        }
      }
    });
  },
  setLabel: function (label) {
    var label = label;

    return this.each(function () {
      if (label.text != undefined && !empty(label.text)) {
        if (label.align === 'left') {
          $(this).prepend(
            $('<span>', {
              class: 'label label-' + label.type,
              html: label.text,
              style: 'margin-left: 8px',
            }),
          );
        } else {
          $(this).append(
            $('<span>', {
              class: 'label label-' + label.type,
              html: label.text,
              style: 'margin-left: 8px',
            }),
          );
        }
      }
    });
  },
  setBadge: function (text) {
    var text = text;

    return this.each(function () {
      if (text != undefined && !empty(text)) {
        $(this).append(
          $('<span>', {
            class: 'badge',
            html: text,
            style: 'margin-left: 8px',
          }),
        );
      }
    });
  },
  setAddOn: function (html, align) {
    var html = html,
      align = align || 'left';

    return this.each(function () {
      if (html != undefined && !empty(html)) {
        var addon = $('<span>', { class: 'input-group-addon' });

        if (typeof html.renderTo === 'function') {
          addon = $('<span>', { class: 'input-group-btn' });
          html.renderTo(addon);
        } else if (typeof html === 'object' && typeof html.xtype != 'undefined') {
          var object = IT.CreateComponent(html);

          addon = $('<span>', { class: 'input-group-btn' });
          object.renderTo(addon);
        } else if (typeof html === 'object') {
          html.appendTo(addon);
        } else {
          if (html.substring(0, 5) == 'icon-') {
            addon.setIcon(html.substring(5, html.length));
          } else if (html.substring(0, 6) == 'check-') {
            addon.append(
              $('<input>', {
                type: 'checkbox',
                name: html.substring(6, html.length),
              }),
            );
          } else if (html.substring(0, 6) == 'radio-') {
            addon.append(
              $('<input>', {
                type: 'radio',
                name: html.substring(6, html.length),
              }),
            );
          } else {
            addon.html(html);
          }
        }

        if (align === 'left') {
          $(this).prepend(addon);
        } else {
          $(this).append(addon);
        }
      }
    });
  },
  setClass: function (value, toggle) {
    var value = value,
      toggle = toggle == false ? false : true;

    return this.each(function () {
      if (value != undefined && !empty(value) && toggle) {
        $(this).addClass(value);
      } else {
        $(this).removeClass(value);
      }
    });
  },
  autoGrow: function () {
    return this.each(function () {
      var $element = $(this).get(0);

      $element.style.overflow = 'hidden';
      $element.style.height = 0;
      $element.style.height = this.scrollHeight + 'px';

      $element.addEventListener(
        'keyup',
        function () {
          this.style.overflow = 'hidden';
          this.style.height = 0;
          this.style.height = this.scrollHeight + 'px';
        },
        false,
      );
    });
  },
  clearElement: function () {
    return this.each(function () {
      //   if (
      //     tinymce != undefined &&
      //     tinymce.editors &&
      //     tinymce.editors.length > 0
      //   ) {
      //     for (var i = tinymce.editors.length - 1; i >= 0; i--) {
      //       tinymce.editors[i].remove();
      //     }
      //   }

      $(this).empty();
    });
  },
});
Object.assign(window, {
  cookie: {
    set: function (name, value, expiredays = 1) {
      var name = 'COOKIE_' + uniqueID + '_' + name;
      var exDate = new Date();
      exDate.setDate(exDate.getDate() + expiredays);
      document.cookie =
        name + '=' + escape(value) + (expiredays == null ? '' : ';expires=' + exDate.toGMTString()) + ';path=/';
    },

    get: function (name) {
      var name = 'COOKIE_' + uniqueID + '_' + name;
      if (document.cookie.length > 0) {
        start = document.cookie.indexOf(name + '=');
        if (start != -1) {
          start = start + name.length + 1;
          end = document.cookie.indexOf(';', start);
          if (end == -1) end = document.cookie.length;
          return unescape(document.cookie.substring(start, end));
        }
      } else {
        return '';
      }
    },
  },
  tools: {
    stringify: function (arObject) {
      var type = typeof arObject;

      if (type == 'object' || arObject === null) {
        return type == 'string' ? '"' + arObject + '"' : arObject;
      } else {
        var index,
          value,
          json = [],
          isArray = arObject && arObject.constructor == Array;

        for (index in arObject) {
          value = arObject[index];
          type = typeof value;

          if (arObject.hasOwnProperty(index)) {
            if (type == 'string') {
              value = '"' + value + '"';
            } else if (type == 'object' && value !== null) {
              value = tools.stringify(value);
            }

            json.push((isArray ? '' : '"' + index + '":') + String(value));
          }
        }

        return (isArray ? '[' : '{') + String(json) + (isArray ? ']' : '}');
      }
    },

    makeRandomId: function () {
      var id = '',
        possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

      for (var i = 0; i < 5; i++) {
        id += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      return 'IT-' + id;
    },

    getScrollBarWidth: function () {
      var outer = document.createElement('div');

      outer.style.visibility = 'hidden';
      outer.style.width = '100px';

      document.body.appendChild(outer);

      var widthNoScroll = outer.offsetWidth;

      outer.style.overflow = 'scroll';

      var inner = document.createElement('div');

      inner.style.width = '100%';
      outer.appendChild(inner);

      var widthWithScroll = inner.offsetWidth;

      outer.parentNode.removeChild(outer);

      return widthNoScroll - widthWithScroll;
    },

    generateURL: function (URL, params) {
      var URL = URL + '?';

      if (typeof params == 'object') {
        $.each(params, function (key, value) {
          URL += key + '=' + value + '&';
        });
      }

      URL = URL + '_dc=' + new Date().getTime();

      return URL;
    },
  },
  empty: function (value) {
    return value == '' || value == undefined || value == null ? true : false;
  },
  implement: function () {
    // console.info(arguments);
    if (arguments.length > 1) {
      for (var i = 1; i < arguments.length; i++) {
        arguments[i].call(arguments[0]);
      }
    }
  },
});
