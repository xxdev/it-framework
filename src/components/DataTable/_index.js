/* eslint-disable no-console */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-new */
/* eslint-disable no-redeclare */
export default function DataTable(params) {
  implement(this, IT.BaseComponent, IT.EventListener);

  var me = this;
  var changeDate = false;
  var i;

  me.onItemClick = (act) => {
    me.addEvent('onItemClick', act);
  };
  me.onItemDblClick = (act) => {
    me.addEvent('onItemDblClick', act);
  };

  me.container = $('<div>', { class: 'table-responsive' });
  me.wrapper = $('<div>', { class: 'table-wrapper' });
  me.fixed = $('<div>', { class: 'table-fixed' });
  me.element = $('<table>', { class: 'table' });
  me.pagination = $('<nav>', { class: 'navbar navbar-default navbar-pagination' });
  me.head = $('<thead>');
  me.body = $('<tbody>');

  me.data = null;
  me.store = null;
  me.params = null;
  me.rowsCount = 0;
  me.pageCount = 0;
  me.page = 1;
  me.start = 0;
  me.limit = 20;
  me.orderBy = null;
  me.sortBy = null;

  me.selectPage = null;
  me.selectedRow = null;
  me.selectedColumn = null;
  me.selectedRecord = null;
  me.lastRow = null;

  me.init = (par) => {
    me.settings = $.extend(
      {},
      IT.defaultSettings,
      {
        id: '',
        autoLoad: false,
        fullSize: false,
        wrap: false,
        dragable: false,
        bordered: true,
        striped: true,
        pagination: 'advanced',
        sort: 'local',
        store: {
          type: 'json',
          params: {
            start: 0,
            limit: 20,
            orderBy: '',
            sortBy: '',
          },
        },
        columns: [],
      },
      par,
    );

    if (typeof me.settings.store === 'function') {
      me.store = me.settings.store;
      me.data = me.store.getData();
    } else {
      me.store = new IT.Store(me.settings.store);
    }

    me.store.onLoad((data, pars) => {
      me.data = data;
      me.params = pars;
      me.start = me.params.start;
      me.limit = me.params.limit;
      me.page = me.start / me.limit + 1;
      me.load();
    });

    me.store.onError((err) => {
      throw new Error(err.message);
    });
  };

  me.load = () => {
    me.body.empty();

    if (me.data !== null) {
      me.rowsCount = me.data.rows.length;
      me.totalRows = me.data.totalRows;

      me.setCaption();

      for (var k = 0; k < me.rowsCount; k += 1) {
        var row = me.data.rows[k],
          tr = $('<tr>');

        if (row.highlight !== undefined) {
          tr.addClass(row.highlight);
        }

        me.createColumn(tr, row, k);
        me.body.append(tr);
      }

      me.createListener();
    }

    if (me.selectPage !== null) me.selectPage.setValue(me.limit);
  };

  me.clear = () => {
    me.store.storeData = { totalRows: 0, rows: [] };
    me.data = { totalRows: 0, rows: [] };

    me.load();
  };

  me.addRow = (data, focus) => {
    focus = focus || 0;
    me.data.rows.push(data);
    me.data.totalRows += 1;
    me.store.storeData.rows.push(data);

    var k = me.data.rows.length - 1,
      row = me.data.rows[k],
      tr = $('<tr>');

    me.createColumn(tr, row, k);
    me.body.append(tr);
    me.createListener();

    me.body.find('tr').last().find('td').eq(focus).click();
  };

  me.createListener = () => {
    me.body
      .children('tr')
      .children('td')
      .click(() => {
        me.body.find('tr').removeClass('selected');
        $(this).parent().addClass('selected');

        var rowIndex = $(this).parent().index(),
          index = $(this).index();

        me.selectedRow = rowIndex;
        me.selectedColumn = index;
        me.selectedRecord = me.data.rows[rowIndex];

        var editor = me.settings.columns[index].editor || null;
        var locked = me.selectedRecord.locked || false;

        if (editor && locked === false && $(this).attr('disabled') === undefined) {
          if (editor.editable || editor.editable === undefined) {
            me.createEditor($(this));
          }
        }

        if (me.selectedRow !== me.lastRow || me.settings.dragable === false) {
          me.fireEvent('onItemClick', [me.selectedRecord]);
          me.lastRow = me.selectedRow;
        }
      });

    me.body
      .children('tr')
      .children('td')
      .dblclick(() => {
        me.fireEvent('onItemDblClick', [me.selectedRecord]);
      });
  };

  me.createEditor = (cell, columnIndex, rowIndex, row) => {
    var columnIndex = columnIndex === undefined ? me.selectedColumn : columnIndex,
      rowIndex = rowIndex || 0,
      row = row || {},
      column = me.settings.columns[columnIndex],
      { editor } = column;

    if (editor.padding === undefined) {
      editor.padding = true;
    }

    me.element.find('select').trigger('select2-blur');

    if (editor.xtype) {
      if (cell.find('.table-editor').length < 1) {
        if (editor.xtype === 'checkbox') {
          editor.inline = true;
          editor.dataIndex = column.dataIndex + rowIndex;
        }

        editor.onTable = true;
        editor.autoGrow = true;
        editor.emptyText = '-';
        editor.align = column.align;
        editor.onClose = () => {
          changeDate = true;
        };

        var component = IT.CreateComponent(editor),
          wrapper = $('<div>', { class: 'table-editor' }),
          value = cell.find('.table-cell').attr('value'),
          text = cell.find('.table-cell').html();

        if (column.align !== undefined) {
          wrapper.addClass(`align-${column.align}`);
        }
        wrapper.addClass();
        component.renderTo(wrapper);

        if (
          (editor.editable !== undefined && editor.editable === false) ||
          (row.locked !== undefined && row.locked === true)
        ) {
          component.setDisabled(true);
        }

        cell.setClass('no-padding', !editor.padding);
        cell.find('.table-cell').hide();
        cell.append(wrapper);

        if (editor.xtype === 'textbox' || editor.xtype === 'combobox') {
          cell.addClass('table-editing');
          cell.removeClass('table-changed');

          component.setValue(value || text);
          component.focus();
          component.getElement().keydown((e) => {
            me.tabPressed(e, $(this));
          });

          var blur = component.getSettings().select2 ? 'select2-blur' : 'blur';
          component.getElement().on(blur, () => {
            var v = $(this).val(),
              changed = me.store.getChanged(me.selectedRow, me.settings.columns[columnIndex].dataIndex, v);
            text = $(this).find('option').filter(':selected').text();
            text = text === '-' ? '' : text;

            if (changeDate || editor.type !== 'date') {
              cell.removeClass('table-editing');
              cell.find('.table-editor').remove();
              cell.find('.table-cell').html(text || v);
              cell.find('.table-cell').attr('value', v);
              cell.find('.table-cell').show();

              if (changed) {
                cell.addClass('table-changed');
              }
            }

            if (changed && typeof editor.allowBlank !== 'undefined' && !editor.allowBlank && value === '') {
              new IT.MessageBox({
                size: 'modal-sm',
                type: 'critical',
                title: 'Peringatan',
                text: `${column.header} tidak boleh kosong`,
              });
            }
          });
        } else if (editor.xtype === 'checkbox') {
          component.setValue(value || text);
          component.addClass(column.dataIndex);

          if (component.getSettings().type !== 'static') {
            component.getElement().click(() => {
              var v = component.getValue();

              changed = me.store.getChanged(rowIndex, me.settings.columns[columnIndex].dataIndex, v);
              if (changed) {
                cell.addClass('table-changed');
              } else {
                cell.removeClass('table-changed');
              }
            });
          }
        }
      }
    }
  };

  me.createColumn = (tr, row, rowIndex) => {
    for (i = 0; i < me.settings.columns.length; i += 1) {
      if (me.settings.columns[i]) {
        var data = row[me.settings.columns[i].dataIndex],
          column = me.settings.columns[i],
          editor = column.editor !== undefined ? column.editor : null,
          { dataIndex } = column;

        var td = $('<td>', { align: 'left' }),
          div = $('<div>', { class: 'table-cell', value: data });

        if (data === undefined || data === null) {
          data = '';
        }

        if ((editor && editor.xtype && editor.xtype === 'combobox') || column.data !== undefined) {
          var arrayData = (editor && (editor.data || editor.datasource.data)) || column.data || [],
            arrayIndex = null;

          for (var z = 0; z < arrayData.length; z += 1) {
            if (arrayData[z].key === data) {
              arrayIndex = z;
              break;
            }
          }

          data = arrayIndex !== null ? arrayData[arrayIndex].value : '';
        }

        div.setCss('text-align', column.align);
        div.setCss('width', column.width);
        div.html(data);

        if (row.disabled !== undefined) {
          var dis = row.disabled[dataIndex];
          if (dis !== undefined) {
            td.addClass('disabled');
            td.attr('disabled', true);
          }
        }

        if (typeof column.cls === 'string') td.addClass(column.cls);
        else if (typeof column.cls === 'function') {
          td.addClass(column.cls(row));
        }

        td.setAttr('width', column.width);
        td.setAttr('align', column.align);
        td.setAttr('valign', column.valign);
        td.append(div);

        if (editor && editor.xtype !== 'textbox' && editor.xtype !== 'combobox') {
          me.createEditor(td, i, rowIndex, row);
        }

        tr.append(td);
      }
    }
  };

  me.setColumnDataSource = (c) => {
    var { editor } = me.settings.columns[c];

    if (editor !== undefined && editor.datasource !== undefined && editor.datasource.type === 'ajax') {
      $.ajax({
        method: 'POST',
        dataType: 'json',
        url: editor.datasource.url,
        data: editor.datasource.params,
        success: (a) => {
          if (!a.success && a.redirect !== undefined) {
            window.location.href = a.redirect;
          }

          editor.datasource = { type: 'array', data: a.rows };
          me.load();
        },
      });
    }
  };

  me.setComboDataSource = (c, datasource) => {
    for (i = 0; i < me.settings.columns.length; i += 1) {
      if (me.settings.columns[i].dataIndex === c) {
        // var column = me.settings.columns[i];
        if (datasource.type === 'ajax') {
          $.ajax({
            method: 'POST',
            dataType: 'json',
            url: datasource.url,
            data: datasource.params,
            // success: (a) => {
            //   if (!a.success && a.redirect !== undefined) {
            //     parent.location = a.redirect;
            //   }

            //   column.editor.datasource = { type: 'array', data: a.rows };
            //   me.load();
            // },
          });
        } else {
          me.settings.columns[i].editor.datasource = datasource;
        }
      }
    }
  };

  me.createHeader = () => {
    if (me.settings.customHeader !== undefined) {
      me.head.html(me.settings.customHeader);
      me.head.addClass('custom-header');

      me.head.find('th').each(() => {
        var width = $(this).attr('width');
        var div = $('<div>', { html: $(this).html() }).css('width', width);

        $(this).html(div);
      });

      for (i = 0; i < me.settings.columns.length; i += 1) {
        me.setColumnDataSource(i);
      }
    } else {
      var tr = $('<tr>');

      for (i = 0; i < me.settings.columns.length; i += 1) {
        var column = me.settings.columns[i];
        if (column) {
          var div = $('<div>'),
            sort = $('<a>', {
              class: 'sort-table',
              href: 'javascript:void(0)',
              'data-index': column.dataIndex,
              'data-sort': '',
            }),
            th = $('<th>');

          me.setColumnDataSource(i);

          div.setCss('width', parseInt(column.width, 10) - th.css('padding'));
          div.setCss('text-align', me.settings.headerAlign);
          th.setCss('width', column.width);
          th.addClass(column.cls);

          sort.append(column.header);
          div.append(sort);
          th.append(div);
          tr.append(th);

          if ((typeof column.sort !== 'undefined' && column.sort === false) || me.settings.sort === 'none') {
            sort.off('click');
          } else {
            // sort.click(() => {
            //   var dataIndex = $(this).data('index'),
            //     dataSort = $(this).attr('data-sort');
            //   $(this).find('.fa').remove();
            //   $(this).closest('thead').find('.fa').remove();
            //   if (dataSort !== 'ASC') {
            //     $(this).setAttr('data-sort', 'ASC');
            //     $(this).setIcon('caret-up', 'right');
            //     me.orderBy = dataIndex;
            //     me.sortBy = 'ASC';
            //   } else {
            //     $(this).setAttr('data-sort', 'DESC');
            //     $(this).setIcon('caret-down', 'right');
            //     me.orderBy = dataIndex;
            //     me.sortBy = 'DESC';
            //   }
            //   if (me.settings.sort === 'local') {
            //     me.store.sort(dataIndex, me.sortBy);
            //   } else {
            //     me.store.load({ params: { orderBy: me.orderBy, sortBy: me.sortBy } });
            //   }
            // });
          }
        }
      }

      me.head.append(tr);
    }

    me.head.appendTo(me.element);
  };

  me.createPagination = () => {
    var formLeft = $('<form>', { class: 'navbar-form navbar-left' }),
      formRight = $('<form>', { class: 'navbar-form navbar-right hidden-xs' }),
      groupLeft = $('<div>', { class: 'form-group' }),
      groupRight = $('<div>', { class: 'form-group' });

    var dataPage = [];
    [10, 20, 30, 50, 100, 200, 500].forEach((id) => {
      dataPage.push({ key: id, value: id });
    });

    // me.selectPage = new IT.ComboBox({
    //   data: dataPage,
    //   renderTo: groupRight,
    //   select2: false,
    //   onChange: () => {
    //     me.limit = parseInt(this.getValue(), 10);
    //     me.store.load({ params: { limit: me.limit } });
    //   },
    //   onLoad: () => {
    //     this.setValue(me.limit);
    //   },
    // });

    groupRight.prepend($('<span>', { class: 'form-control-static hidden-xs', text: 'Jumlah Per Halaman' }));
    groupLeft.append(
      $('<a>', { class: 'btn btn-default', href: 'javascript:void(0)', id: 'table-first' }).setIcon('resultset-first'),
    );
    groupLeft.append(
      $('<a>', { class: 'btn btn-default', href: 'javascript:void(0)', id: 'table-previous' }).setIcon(
        'resultset-previous',
      ),
    );
    groupLeft.append($('<input>', { class: 'form-control', href: 'javascript:void(0)', id: 'table-page', value: '1' }));
    groupLeft.append(
      $('<span>', { class: 'form-control-static', href: 'javascript:void(0)', id: 'table-page-count', text: 'dari 1' }),
    );
    groupLeft.append(
      $('<a>', { class: 'btn btn-default', href: 'javascript:void(0)', id: 'table-next' }).setIcon('resultset-next'),
    );
    groupLeft.append(
      $('<a>', { class: 'btn btn-default', href: 'javascript:void(0)', id: 'table-last' }).setIcon('resultset-last'),
    );
    groupLeft.append(
      $('<a>', { class: 'btn btn-default', href: 'javascript:void(0)', id: 'table-refresh' }).setIcon('arrow-refresh'),
    );
    groupLeft.append(
      $('<span>', {
        class: 'form-control-static hidden-xs',
        href: 'javascript:void(0)',
        id: 'table-show',
        text: 'Tidak ada Data untuk ditampilkan',
      }),
    );

    groupLeft.find('a').click(() => {
      var id = $(this).attr('id');
      me.setPage(id);
    });

    groupLeft.find('input').keypress((e) => {
      if (e.which === 13) {
        e.preventDefault();

        var value = parseInt($(this).val(), 10);

        if (isNaN(value) === true) {
          me.loadPage(value);
        }
      }
    });

    formLeft.append(groupLeft);
    formRight.append(groupRight);

    me.pagination.append(formLeft);
    me.pagination.append(formRight);

    if (me.settings.pagination === 'simple') {
      me.pagination.find('.hidden-xs').hide();
    }
  };

  me.createComponent = () => {
    me.createHeader();
    me.initLayout(me.container);
    me.element.append(me.header);
    me.element.setId(me.settings.id);
    me.element.setClass('wrap', me.settings.wrap);
    me.element.setClass('table-bordered', me.settings.bordered);
    me.element.setClass('table-striped', me.settings.striped);

    if (me.settings.fixedHeader) {
      me.fixed.append(me.element.clone(true, true));
      me.fixed.setCss('margin-right', tools.getScrollBarWidth());
      me.wrapper.setCss('overflow-y', 'scroll');
      // me.wrapper.setCss('position', 'absolute');
    } else {
      me.wrapper.setCss('position', 'relative');
    }

    if (me.settings.pagination !== 'hidden') {
      me.createPagination();
      me.container.append(me.pagination);
    } else {
      me.wrapper.setCss('margin-bottom', '0');
    }

    me.element.append(me.body);
    me.wrapper.append(me.element);
    me.wrapper.scroll(() => {
      me.fixed.scrollLeft($(this).scrollLeft());
    });

    me.container.append(me.wrapper);
    me.container.append(me.fixed);
    me.container.setCss('width', me.settings.width);
    me.container.setCss('height', me.settings.height);
    me.container.setClass('fullsize', me.settings.fullSize);

    if (me.settings.autoLoad) {
      me.store.load();
    }
  };

  me.showError = (arError) => {
    var changed = me.store.dataChanged;
    for (i = 0; i < changed.length; i += 1) {
      if (arError.indexOf(changed[i].indexRow.toString()) === -1) {
        me.body.find('tr').eq(changed[i].indexRow).find('td').removeClass('table-changed');
        me.body.find('tr').eq(changed[i].indexRow).removeClass('danger');
        changed.slice(i);
      } else {
        me.body.find('tr').eq(changed[i].indexRow).setClass('danger');
      }
    }
  };

  //   me.tabPressed = (e, o) => {
  //     var columnIndex = o.closest('td').index();
  //     var columnCount = o.closest('tr').find('td').length;
  //     var nextIndex;
  //     if (e.keyCode === 9 && !e.shiftKey && columnIndex + 1 < columnCount) {
  //       nextIndex = columnIndex + 1;
  //       for (var i = columnIndex + 1; i < me.settings.columns.length; i += 1) {
  //         var editor = me.settings.columns[i].editor;
  //         if (editor && (editor.xtype === 'textbox' || editor.xtype === 'combobox')) {
  //           nextIndex = i;
  //           break;
  //         }
  //       }

  //       e.preventDefault();
  //       next = o.closest('tr').find(`td:eq(${nextIndex})`);
  //       o.blur();
  //       next.click();
  //     } else if (e.keyCode === 9 && e.shiftKey && columnIndex - 1 >= 0) {
  //       var nextIndex = columnIndex - 1;
  //       for (var i = columnIndex - 1; i > me.settings.columns.length; i--) {
  //         var editor = me.settings.columns[i].editor;
  //         if (editor && (editor.xtype === 'textbox' || editor.xtype === 'combobox')) {
  //           nextIndex = i;
  //           break;
  //         }
  //       }

  //       e.preventDefault();
  //       next = o.closest('tr').find('td:eq(' + nextIndex + ')');
  //       o.blur();
  //       next.click();
  //     } else if (e.keyCode === 38 && me.selectedRow - 1 >= 0) {
  //       e.preventDefault();
  //       next = o
  //         .closest('tbody')
  //         .find('tr:eq(' + (me.selectedRow - 1) + ')')
  //         .find('td:eq(' + columnIndex + ')');
  //       o.blur();
  //       next.click();
  //     } else if (e.keyCode === 40 && me.selectedRow + 1 < me.data.rows.length) {
  //       e.preventDefault();
  //       next = o
  //         .closest('tbody')
  //         .find('tr:eq(' + (me.selectedRow + 1) + ')')
  //         .find('td:eq(' + columnIndex + ')');
  //       o.blur();
  //       next.click();
  //     }
  //   };

  me.loadPage = (p) => {
    if (me.data !== null) {
      me.page = p;
      me.start = (p - 1) * me.limit;
      me.store.load({ params: { start: me.start, limit: me.limit } });
    }
  };

  me.setPage = (act) => {
    me.pageCount = Math.ceil(me.totalRows / me.limit);

    switch (act) {
      case 'table-first':
        if (me.page !== 1) me.loadPage(1);
        break;
      case 'table-last':
        if (me.page !== me.pageCount) me.loadPage(me.pageCount);
        break;
      case 'table-next':
        if (me.page < me.pageCount) me.loadPage(me.page + 1);
        break;
      case 'table-previous':
        if (me.page > 1) me.loadPage(me.page - 1);
        break;
      case 'table-refresh':
        me.store.load();
        break;
      default:
        break;
    }
  };

  me.setCaption = () => {
    var start = me.start + 1,
      end = me.start + me.limit;

    if (me.totalRows > 0) {
      end = end > me.totalRows ? me.totalRows : end;
      me.pageCount = Math.ceil(me.totalRows / me.limit);

      me.pagination.find('#table-page').val(me.page);
      me.pagination.find('#table-page-count').html(`dari ${me.pageCount}`);
      me.pagination.find('#table-show').html(`Menampilkan data ${start}-${end} dari ${me.totalRows}`);
    } else {
      me.pagination.find('#table-page').val('');
      me.pagination.find('#table-page-count').html('dari');
      me.pagination.find('#table-show').html('Tidak ada data untuk ditampilkan');
    }
  };

  me.getChanged = () => {
    if (me.store.dataChanged !== undefined && me.store.dataChanged.length > 0) {
      return me.store.dataChanged;
    }
    return null;
  };

  me.renderTo = (p) => {
    if (!empty(p)) {
      me.container.appendTo(p);
      me.parrent = p;
    }
  };

  me.init(params);
  me.createComponent();
  me.renderTo(me.settings.renderTo);

  return me;
}
