export default function InputComponent() {
  var me = this;

  me.base = null;
  me.container = $('<div>', { class: 'input-group' });

  me.setSize = (c) => {
    var size = c || me.settings.size;
    if (!empty(size)) {
      me.base.wrap($('<div>', { class: size }));
    }
  };

  me.renderTo = (p) => {
    if (!empty(p)) {
      if (!empty(me.settings.leftAddon) || !empty(me.settings.rightAddon)) {
        me.container.append(me.element);
        me.container.setAddOn(me.settings.leftAddon, 'left');
        me.container.setAddOn(me.settings.rightAddon, 'right');
        me.container.appendTo(p);
        me.base = me.container;
      } else {
        me.element.appendTo(p);
        me.base = me.element;
      }

      // me.base.appendTo(p);
      me.setSize();
      me.parent = p;
    }
  };
}
