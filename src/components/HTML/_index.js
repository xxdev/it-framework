export default function HTML(params) {
  var me = this;
  implement(this, IT.BaseComponent);

  me.element = $('<div>', { class: 'html-object' });

  me.init = (args) => {
    me.settings = $.extend({}, IT.defaultSettings, { content: '' }, args);
  };

  me.getContent = () => {
    return me.element.html();
  };

  me.setContent = (content) => {
    return me.element.html(content);
  };

  me.createComponent = () => {
    me.initComponent(me.element);
    me.element.html(me.settings.content);
  };

  me.init(params);
  me.createComponent();
  me.renderTo(me.settings.renderTo);

  return me;
}
