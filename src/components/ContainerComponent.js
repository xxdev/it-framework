export default function ContainerComponent() {
  var me = this;

  me.getComponent = (item) => {
    if (typeof item.renderTo === 'function') {
      return item;
    }
    if (typeof item === 'object') {
      return IT.CreateComponent(item);
    }
    return false;
  };

  me.getItemId = (item, idx) => {
    if (typeof item.getId === 'function') {
      return item.getId();
    }
    return idx;
  };

  me.getItem = (i) => {
    if (typeof i === 'string') {
      return me.itemObject[i];
    }
    return me.itemArray[i];
  };

  me.addItem = (item, index) => {
    if (typeof item.itemObject === 'object') {
      $.extend(me.itemObject, item.itemObject);
    }

    me.itemArray[index] = item;
    me.itemObject[me.getItemId(item, index)] = item;
  };

  me.setHeader = (header) => {
    if (empty(header)) {
      me.header.hide();
      me.body.removeClass('with-header');
    } else {
      me.body.addClass('with-header');
      me.header.html(header);
      me.header.show();
    }
  };

  me.setFooter = (footer) => {
    if (empty(footer)) {
      me.footer.hide();
      me.body.removeClass('with-footer');
    } else if (typeof footer === 'string') {
      me.body.addClass('with-footer');
      me.footer.html(footer);
      me.footer.show();
    } else {
      me.body.addClass('with-footer');
      footer = me.getComponent(footer);
      footer.renderTo(me.footer);

      me.footer.show();
    }
  };

  me.setValue = (i, v) => {
    var item = me.getItem(i);
    if (item && typeof item.setValue === 'function') {
      item.setValue(v);
    }
  };

  me.getValue = (i) => {
    var item = me.getItem(i),
      value = '';

    if (item && typeof item.getValue === 'function') {
      value = item.getValue();
    }

    return value;
  };
}
