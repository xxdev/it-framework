export default function CheckBox(params) {
  implement(this, IT.EventListener, IT.BaseComponent, IT.InputComponent, IT.DataSource);

  var me = this;
  var check;
  me.element = $('<div>', { class: 'check' });
  me.data = [];
  me.params = {};

  me.onLoad = (act) => {
    me.addEvent('onLoad', act);
  };
  me.onChange = (act) => {
    me.addEvent('onChange', act);
  };
  me.onComplete = (act) => {
    me.addEvent('onComplete', act);
  };

  me.init = (par) => {
    me.settings = $.extend(
      {},
      IT.defaultSettings,
      {
        dataIndex: 'textbox',
        allowBlank: true,
        type: 'checkbox',
        value: '',
        inline: false,
        autoLoad: true,

        datasource: {
          type: '',
          data: null,
          url: '',
        },
      },
      par,
    );

    if (me.settings.data) {
      me.settings.datasource = {
        type: 'array',
        data: [me.settings.data],
      };
    }
  };

  me.createComponent = () => {
    me.settings.id = me.settings.dataIndex;
    me.initComponent(me.element);

    me.element.find('input').click(() => {
      if (me.isValid() === false) $(this).parent().parent().addClass('has-error');
      else $(this).parent().parent().removeClass('has-error');
    });
    me.element.find('input').change(() => {
      me.fireEvent('onChange', ['a', 'b']);
    });

    if (me.settings.autoLoad) me.getDataSource();
  };

  me.processDatasource = () => {
    me.element.empty();

    var row = me.data;
    for (var i = 0; i < row.length; i += 1) {
      var text = me.settings.format != null ? me.settings.format.format(row[i].key, row[i].value) : row[i].value;
      var dataParams = typeof row[i].params !== 'undefined' ? row[i].params : '';

      var cek = row[i].key === me.settings.value;
      var wrapper = $('<div>', { class: `${me.settings.type} ${me.settings.class}` });
      var label = $('<label>', { html: '&nbsp;', for: me.settings.dataIndex + row[i].key });

      if (me.settings.type === 'static') {
        check = $('<span>', {
          style: 'font-size: 15px',
          class: `fa fa-${cek ? 'check-circle text-green' : 'times-circle text-red'}`,
        }).setDisable(me.settings.disabled);
      } else {
        check = $('<input>', {
          class: 'styled cek',
          type: me.settings.type,
          name: me.settings.dataIndex + (me.settings.type === 'checkbox' && row.length > 1 ? '[]' : ''),
          id: me.settings.dataIndex + row[i].key,
          value: row[i].key,
          checked: cek,
        })
          .setAttr('data-params', dataParams)
          .setDisable(me.settings.disabled)
          .click(() => {
            me.isValid();
          });
      }

      if (me.settings.inline) {
        wrapper.removeAttr('class');
        wrapper.addClass(`${me.settings.type}-inline`);
      }

      wrapper.append(check);

      if (!empty(text)) {
        wrapper.append(label.append(text));
      } else {
        check.css('margin-right', 0);
        check.css('position', 'relative');
      }

      me.element.append(wrapper);
    }

    setTimeout(() => {
      me.fireEvent('onLoad', [row]);
      me.fireEvent('onComplete', [row]);
    }, 1);
  };

  me.setDisabled = (d) => {
    me.element.setDisable(d);
    me.element.find('input').setDisable(d);
  };

  me.getValue = () => {
    var value = me.element.find('input').filter(':checked').val();
    if (value) {
      return value;
    }
    return '';
  };

  me.setValue = (v) => {
    if (empty(v)) {
      me.element.find('input:checked').prop('checked', false);
      me.element.find('span').removeClass('text-green');
      me.element.find('span').removeClass('fa-check-circle');
      me.element.find('span').addClass('text-red');
      me.element.find('span').addClass('fa-times-circle');
    } else {
      var value = `${v}`;
      var values = value.split(',');

      $.each(values, (index, va) => {
        me.element.find(`input[value="${va}"]`).prop('checked', true);
      });

      me.element.find('span').removeClass('text-red');
      me.element.find('span').removeClass('fa-times-circle');
      me.element.find('span').addClass('text-green');
      me.element.find('span').addClass('fa-check-circle');
    }
  };

  me.setType = (t) => {
    me.settings.type = t;
    me.processDatasource();
  };

  me.getParams = () => {
    return me.element.find('input:checked').data('params');
  };

  me.getItem = (v) => {
    return me.element.find(`input[value="${v}"]`);
  };

  me.isValid = () => {
    var valid = !(me.settings.allowBlank === false && empty(me.getValue()));

    if (!valid) {
      me.element.parent().addClass('has-error');
      return false;
    }
    me.element.parent().removeClass('has-error');
    return true;
  };

  me.init(params);
  me.createComponent();
  me.renderTo(me.settings.renderTo);

  return me;
}
