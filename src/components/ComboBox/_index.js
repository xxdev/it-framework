export default function ComboBox(params) {
  implement(this, IT.EventListener, IT.BaseComponent, IT.InputComponent, IT.DataSource);

  var me = this;

  me.element = $('<select>', { class: 'form-control' });
  me.data = [];
  me.params = {};

  me.onLoad = (act) => {
    me.addEvent('onLoad', act);
  };
  me.onComplete = (act) => {
    me.addEvent('onComplete', act);
  };
  me.onChange = (act) => {
    me.element.change(act);
  };
  me.change = () => {
    me.element.change();
  };

  me.init = (par) => {
    me.settings = $.extend(
      {},
      IT.defaultSettings,
      {
        dataIndex: 'combobox',
        allowBlank: true,
        leftAddon: '',
        rightAddon: '',
        value: '',
        size: '',
        autoLoad: true,
        showEmptyText: true,
        emptyText: '',
        onTable: false,
        select2: false,
        line: false,
        autoSelect: true,
        datasource: {
          type: '',
          data: null,
          url: '',
        },
      },
      par,
    );

    if (me.settings.data) {
      me.settings.datasource = {
        type: 'array',
        data: me.settings.data,
      };
    }
  };

  me.createComponent = () => {
    me.settings.id = me.settings.dataIndex;
    me.initComponent(me.element);

    me.element.setAttr('name', me.settings.dataIndex);
    me.element.blur(() => {
      me.isValid();
    });

    me.element.change(() => {
      me.fireEvent('onChange', ['a', 'b']);
    });

    var blur = me.settings.select2 ? 'select2-blur' : 'blur';
    me.element.on(blur, () => {
      me.isValid();
    });

    if (me.settings.autoLoad) me.getDataSource();
  };

  me.processDatasource = () => {
    me.element.empty();

    // if (me.settings.emptyText || me.settings.onTable) {
    if (me.settings.showEmptyText && me.settings.emptyText) {
      me.element.append($('<option>', { value: '', text: me.settings.emptyText }));
    }

    var row = me.data;
    for (var i = 0; i < row.length; i += 1) {
      var text = me.settings.format !== undefined ? me.settings.format.format(row[i].key, row[i].value) : row[i].value;
      var dataParams = typeof row[i].params !== 'undefined' ? row[i].params : '';

      var sel = row[i].key === me.settings.value;
      var option = $('<option>', {
        value: row[i].key,
        selected: sel,
        text,
      }).setAttr('data-params', dataParams);

      me.element.append(option);
    }

    setTimeout(() => {
      if (typeof row !== 'undefined' && row.length && row.length === 1 && me.settings.autoSelect) {
        me.element.find('option[value=""]').remove();
        me.element.val(row[0].key);
        // me.element.change();
      }

      if (me.settings.select2) {
        var width = {};

        width = me.settings.onTable ? { width: me.element.parent().width() } : width;
        width = me.element.parent().parent().hasClass('navbar-form') ? { width: 'resolve' } : {};

        if (me.settings.line) {
          me.element.closest('.select2-container').addClass('has-line');
        }

        me.element.select2(width);
      }

      me.fireEvent('onLoad', [row]);
      me.fireEvent('onComplete', [row]);
    }, 1);
  };

  me.clear = () => {
    me.setDataSource({
      type: 'array',
      data: [],
    });
  };

  me.setEmptyText = (opt) => {
    // true||false||string
    me.settings.showEmptyText = !!opt;
    if (typeof opt === 'string') me.settings.emptyText = opt;
  };

  me.getValue = () => {
    return !empty(me.element.val()) ? me.element.val() : me.element.find('option').filter(':selected').val();
  };

  me.getDisplayValue = () => {
    return me.element.find('option').filter(':selected').html();
  };

  me.setValue = (v) => {
    if (me.settings.select2) {
      me.element.select2('val', v);
    }

    me.element.val(v);
  };

  me.getParams = () => {
    return me.element.find('option:selected').data('params');
  };

  me.isValid = () => {
    var valid = !(me.settings.allowBlank === false && empty(me.getValue()));

    if (valid) {
      me.element.parent().removeClass('has-error');
    } else {
      me.element.parent().addClass('has-error');
    }
    return valid;
  };

  me.init(params);
  me.createComponent();
  me.renderTo(me.settings.renderTo);

  return me;
}
