<?php
// header("Content-Type: application/json");
// var_dump($_SERVER);
// exit();

?>


<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="/.favicon.ico">
    <title>Directory Contents</title>

    <link rel="stylesheet" href="/.style.css">
    <script src="/.sorttable.js"></script>
</head>

<body>
    <div id="container">
        <h1>Directory Contents</h1>

        <table class="sortable">
            <thead>
                <tr>
                    <th>Filename</th>
                    <th>Type</th>
                    <th>Size</th>
                    <th>Date Modified</th>
                </tr>
            </thead>
            <tbody><?php

// Adds pretty filesizes
function pretty_filesize($file)
{
    $size = filesize($file);
    if ($size < 1024) {
        $size = $size . " Bytes";
    } elseif (($size < 1048576) && ($size > 1023)) {
        $size = round($size / 1024, 1) . " KB";
    } elseif (($size < 1073741824) && ($size > 1048575)) {
        $size = round($size / 1048576, 1) . " MB";
    } else {
        $size = round($size / 1073741824, 1) . " GB";
    }
    return $size;
}

// Opens directory
$dir = '.' . $_SERVER['PHP_SELF'];
if (substr($dir, -4) == '.php') {
    $dir = './';
}
// Checks to see if veiwing hidden files is enabled
if ($_SERVER['QUERY_STRING'] == "hidden") {
    $hide = "";
    $ahref = "/" . $dir;
    $atext = "Hide";
} else {
    $hide = ".";
    $ahref = "?hidden";
    $atext = "Show";
}

// var_dump($dir);
$myDirectory = opendir($dir);

// Gets each entry
while ($entryName = readdir($myDirectory)) {
    if ($hide == "") {
        $dirArray[] = $entryName;
    } elseif (!in_array($entryName, ['.', '..'])) {
        // $dirArray[] = $_SERVER['REQUEST_URI'] == '/' ? $entryName : substr($dir, 2) . DIRECTORY_SEPARATOR . $entryName;
        $dirArray[] = $entryName;
    }
}
// Closes directory
closedir($myDirectory);

// Counts elements in array
$indexCount = count($dirArray);

// Sorts files
sort($dirArray);

// Loops through the array of files
for ($index = 0; $index < $indexCount; $index++) {
    // Decides if hidden files should be displayed, based on query above.
    if (substr("$dirArray[$index]", 0, 1) != $hide) {

        // Resets Variables
        $favicon = "";
        $class = "file";

        // Gets File Names
        $name = $dirArray[$index];
        $namehref = $_SERVER['REQUEST_URI'] == "/" ? $dirArray[$index] : substr($dir, 1) . DIRECTORY_SEPARATOR . $dirArray[$index];
        $realFilename = getcwd() . '/' . $namehref;

        // Gets Date Modified
        $modtime = date("M j Y g:i A", filemtime($realFilename));
        $timekey = date("YmdHis", filemtime($realFilename));

        // Separates directories, and performs operations on those directories
        if (is_dir($realFilename)) {
            $extn = "&lt;Directory&gt;";
            $size = "&lt;Directory&gt;";
            $sizekey = "0";
            $class = "dir";

            // Cleans up . and .. directories
            if ($name == ".") {
                $name = ". (Current Directory)";
                $extn = "&lt;Directory&gt;";
                $namehref = './?hidden';
            }
            if ($name == "..") {
                $name = ".. (Parent Directory)";
                $namehref = '../?hidden';
                $extn = "&lt;Directory&gt;";
            }
        }

        // File-only operations
        else {
            // Gets file extension
            $extn = pathinfo($dirArray[$index], PATHINFO_EXTENSION);

            // Prettifies file type
            switch ($extn) {
                case "png":
                    $extn = "PNG Image";
                    break;
                case "jpg":
                    $extn = "JPEG Image";
                    break;
                case "jpeg":
                    $extn = "JPEG Image";
                    break;
                case "svg":
                    $extn = "SVG Image";
                    break;
                case "gif":
                    $extn = "GIF Image";
                    break;
                case "ico":
                    $extn = "Windows Icon";
                    break;

                case "txt":
                    $extn = "Text File";
                    break;
                case "log":
                    $extn = "Log File";
                    break;
                case "htm":
                    $extn = "HTML File";
                    break;
                case "html":
                    $extn = "HTML File";
                    break;
                case "xhtml":
                    $extn = "HTML File";
                    break;
                case "shtml":
                    $extn = "HTML File";
                    break;
                case "php":
                    $extn = "PHP Script";
                    break;
                case "js":
                    $extn = "Javascript File";
                    break;
                case "css":
                    $extn = "Stylesheet";
                    break;

                case "pdf":
                    $extn = "PDF Document";
                    break;
                case "xls":
                    $extn = "Spreadsheet";
                    break;
                case "xlsx":
                    $extn = "Spreadsheet";
                    break;
                case "doc":
                    $extn = "Microsoft Word Document";
                    break;
                case "docx":
                    $extn = "Microsoft Word Document";
                    break;

                case "zip":
                    $extn = "ZIP Archive";
                    break;
                case "htaccess":
                    $extn = "Apache Config File";
                    break;
                case "exe":
                    $extn = "Windows Executable";
                    break;

                default:
                    if ($extn != "") {
                        $extn = strtoupper($extn) . " File";
                    } else {
                        $extn = "Unknown";
                    }
                    break;
            }

            // Gets and cleans up file size
            $size = pretty_filesize($realFilename);
            $sizekey = filesize($realFilename);
        }

        // Output
        echo ("
		<tr class='$class'>
			<td><a href='$namehref'$favicon class='name'>$name</a></td>
			<td><a href='$namehref'>$extn</a></td>
			<td sorttable_customkey='$sizekey'><a href='$namehref'>$size</a></td>
			<td sorttable_customkey='$timekey'><a href='$namehref'>$modtime</a></td>
		</tr>");
    }
}
?>

            </tbody>
        </table>

        <h2><?php echo ("<a href='$ahref'>$atext hidden files</a>"); ?></h2>
    </div>
</body>

</html>