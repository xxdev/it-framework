module.exports = {
  extends: ['airbnb-base', 'prettier'],
  env: {
    node: true,
    es6: true,
  },
  plugins: ['babel', 'prettier'],
  rules: {
    'prettier/prettier': [
      'error',
      {
        endOfLine: 'auto',
      },
    ],
    'no-var': 0,
    'one-var': 0,
    'vars-on-top': 0,
    'no-param-reassign': 0,
    'no-console': 1,
    'no-undef': 0,
    'no-script-url': 0,
    camelcase: 0,
  },
};
