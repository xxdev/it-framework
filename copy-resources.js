var copydir = require("copy-dir");
var fs = require("fs");
["fonts/google-sans", "fonts/roboto", "fonts/barcode", "img"].forEach((dir) => {
  var dist = "./dist/" + dir;
  console.info("adding fonts : " + dist);
  !fs.existsSync(dist) && fs.mkdirSync(dist, { recursive: true });
  copydir.sync("./src/" + dir, dist);
});
