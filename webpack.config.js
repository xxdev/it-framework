const path = require("path");

module.exports = {
  entry: "./src/it-framework.js",
  mode: "production",
  output: {
    filename: "it-framework.min.js",
    path: path.resolve(__dirname, "dist", "js"),
  },
};
